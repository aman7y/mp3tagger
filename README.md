This script written in python3(linux) is used to tag mp3 files from JioSaavn.com

Mp3's album name, tracknumber, title, artist and albumart/cover is tagged

You need to install python3 on your computer to run the script

In addition to python3 you need the following pip3 packages:
  1.beautifulsoup
  2.selenium (and install geckodriver in os)
  3.mutagen
  4.urllib
  5.eyeD3 (install in os)

Now how to tag a mp3 once you have setup the environment

The default path for mp3 used here is '/home/aman/dwhelper/1.mp3'. You can replace this path with your mp3 path wherever it is saved.

*Note: This script can tag one mp3 at a time.

Once you have downloaded your mp3 and your path is setup

Go to Saavn.com

Search for album of the mp3

Once the album is found copy the url and remember the tracknumber

Paste the url in driver.get('yourUrl') and set the t variable below it to the tracknumber

Now run on command line 'python3 mp3GTv0.3.py'

If nothing goes wrong your mp3 will be tagged

Thanks!
