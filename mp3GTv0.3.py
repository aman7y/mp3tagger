from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import json
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3

TEXT_ENCODING = 'utf8'

mp3 = MP3('/home/user/dwhelper/1.mp3')
durationTagger = int(mp3.info.length)
mp3.delete()
mp3.save()


driver = webdriver.Firefox()
driver.get("https://www.jiosaavn.com/album/jab-we-met/WraOviBrkpo_")
t = 2;
res = driver.execute_script("return document.documentElement.outerHTML")
driver.quit()
soup = BeautifulSoup(res, 'lxml')
sw = soup.findAll("li", {"class": "song-wrap"})

for x in sw:
    if int(x.div.text) == t:
        track = x.div.text
        title = ((((json.loads((x.find("div", {"class": "hide song-json"})).text))['title']).replace('&quot;', '"')).replace("&#039;", "'")).replace('&amp;', '&')
        album = ((((json.loads((x.find("div", {"class": "hide song-json"})).text))['album']).replace('&quot;', '"')).replace("&#039;", "'")).replace('&amp;', '&')
        artist = ((((json.loads((x.find("div", {"class": "hide song-json"})).text))['singers']).replace('&quot;', '"')).replace("&#039;", "'")).replace('&amp;', '&')
        albumArt = ((json.loads((x.find("div", {"class": "hide song-json"})).text))['image_url']).replace("150x150", "500x500")
        break

'''
track = '1'
title = "It Ain't Me"
album = "It Ain't Me"
artist = 'Alan Walker, Sabrina Carpenter'
albumArt = "https://i1.sndcdn.com/artworks-000379640541-63wwjs-t500x500.jpg"
'''


mp3 = MP3('/home/user/dwhelper/1.mp3')
mp3.delete()
mp3.save()

mp3 = MP3('/home/user/dwhelper/1.mp3')
if mp3.tags is None:
    mp3.add_tags()
tags = mp3.tags
mp3.save()

audio = EasyID3('/home/user/dwhelper/1.mp3')
audio['tracknumber'] = track
audio['title'] = title
audio['album'] = album
audio['artist'] = artist
audio.save()

from urllib.request import urlopen
response = urlopen(albumArt)
imagedata = response.read()

import eyed3
file = eyed3.load('/home/user/dwhelper/1.mp3')
file.tag.images.set(3, imagedata, "image/jpeg")
file.tag.save()

import os
os.system("eyeD3 --rename '$album - $track:num - $title - $artist' '/home/user/dwhelper/1.mp3'")
